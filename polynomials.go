package gomath

import (
	"fmt"
	"math"
)

// Differentiable means that the struct implementing it can have its derivative taken.
type Differentiable interface {
	Derivative()
}

// Term represents a single term in an polynomial.
type Term struct {
	Coefficient float64
	Power       int
}

// Derivative will work out the derivative of a term.
func (t Term) Derivative() Term {
	if t.Power <= 0 {
		return Term{0.0, 0}
	}

	return Term{
		Coefficient: t.Coefficient * float64(t.Power),
		Power:       t.Power - 1,
	}

}

// String will return a string representation of the term
func (t Term) String() string {
	var currentString = ""

	strCoefficient := fmt.Sprintf("%.2f", t.Coefficient)
	strPower := fmt.Sprintf("%d", t.Power)

	switch {
	case t.Coefficient == -1:
		currentString += "-"
	case t.Coefficient == 1:
		currentString += "+"
	case t.Coefficient < 0:
		currentString += strCoefficient
	case t.Coefficient > 0:
		currentString += "+" + strCoefficient
	}

	switch {
	case t.Power == 0:
		currentString += ""
	case t.Power == 1:
		currentString += "x"
	default:
		currentString += "x^" + strPower

	}

	return currentString
}

// Polynomial represents a polynomial, such as x^2 - 16
type Polynomial struct {
	Powers []Term
}

// Evaluate will work out the value of the polynomial for a given x.
func (p Polynomial) Evaluate(x float64) float64 {
	var total = 0.0

	for _, term := range p.Powers {
		var termValue = term.Coefficient * math.Pow(x, float64(term.Power))

		total += termValue
	}

	return total
}

// Derivative will return a polynomial's derivative, assuming it has integer powers.
func (p Polynomial) Derivative() Polynomial {
	var newPowers = []Term{}

	for _, term := range p.Powers {
		var derivTerm = term.Derivative()

		if derivTerm.Coefficient == 0.0 {
			continue
		}

		newPowers = append(newPowers, derivTerm)
	}

	return Polynomial{Powers: newPowers}
}

// NewtonRoot will attempt to solve the polynomial by using Newton's method, which will only work if there is a real solution.
func (p Polynomial) NewtonRoot() float64 {
	var currentGuess = 1.0

	var derivP = p.Derivative()

	for i := 0; i < 100; i++ {
		currentGuess -= p.Evaluate(currentGuess) / derivP.Evaluate(currentGuess)
	}

	return currentGuess
}

// String will give a human readable representation of the polynomial.
func (p Polynomial) String() string {
	var currentString = ""

	for _, term := range p.Powers {
		currentString += term.String()
	}

	if string(currentString[0]) == "+" {
		return currentString[1:]
	}

	return currentString
}
