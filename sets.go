package gomath

// Contains returns true if the integer slice contains the integer x.
func Contains(slice []int, x int) bool {
	contains := false

	for _, val := range slice {
		if val == x {
			contains = true
		}
	}

	return contains
}
