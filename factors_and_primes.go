package gomath

import (
	"math"
)

// Factors finds the factors of a number using trial division.
func Factors(n int) []int {
	if n == 1 {
		return []int{1}
	}

	factors := []int{1, n}

	for i := 2; i <= int(math.Ceil(float64(n)/2.0)); i++ {
		if n%i == 0 {
			factors = append(factors, i)
		}
	}

	return factors
}

// IsPrime will check if a number is prime.
func IsPrime(n int) bool {
	sum := 0

	for _, val := range Factors(n) {
		sum += val
	}

	return sum == n+1
}

// smallestPrimeDividing will return the smallest prime that is a factor of n.
func smallestPrimeDividing(n int) int {
	var result int

	for i := 1; i <= n; i++ {
		if n%i == 0 {
			if IsPrime(i) {
				result = i
				break
			}
		}
	}

	return result
}

// PrimeFactors will return the prime factors of an integer.
func PrimeFactors(n int) []int {
	if IsPrime(n) {
		return []int{n}
	} else {
		smallestPrimeDivisor := smallestPrimeDividing(n)
		return append(PrimeFactors(n/smallestPrimeDivisor), smallestPrimeDivisor)
	}
}

// GCD will find the greatest common divisor between two numbers.
func GCD(x int, y int) int {
	for y != 0 {
		temp := x
		x = y
		y = temp % y
	}

	return x
}

func numInSlice(x int, xs []int) bool {
	var inList bool

	for _, val := range xs {
		if val == x {
			inList = true
		}
	}

	return inList
}

// Sieve will find all the prime numbers in a range, including the upper & lower bounds.
func Sieve(lower int, upper int) []int {
	numbers := []int{}
	primes := []int{}
	composites := []int{}

	for i := lower; i <= upper; i++ {
		numbers = append(numbers, i)
	}

	for _, num := range numbers {
		if numInSlice(num, composites) {
			continue
		} else if IsPrime(num) {
			primes = append(primes, num)

			for i := 2 * num; i <= upper; i += num {
				composites = append(composites, i)
			}
		}
	}

	return primes

}
