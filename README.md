# `gomath`
This repository really isn't anything special. In an effort to learn Go, I have re-written some of the functions from an old project of mine (the incorrectly  named [Number-Theory](https://github.com/ollybritton/number-theory)) in Go. Most of the functions here are implemented really badly, like how the integer factorization algorithm uses trial division.

## Installation
Run the following command

    go get https://gitlab.com/ollybritton/gomath

You should now be able to use the package by doing

    import "gitlab.com/ollybritton/gomath"

## Examples
Examples can be found in the `examples` folder.