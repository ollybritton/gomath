package main

import (
	"fmt"

	"gitlab.com/ollybritton/gomath"
)

func main() {
	// Create a new polynomial
	testPoly := gomath.Polynomial{
		Powers: []gomath.Term{
			gomath.Term{1.0, 2},
			gomath.Term{-16.0, 0},
		},
	}

	// Print the new polynomial in a human readable way
	fmt.Println(testPoly)

	// Find the derivative of the polynomial
	derivPoly := testPoly.Derivative()
	fmt.Println(derivPoly)

	// Attempt to find a root of the polynomial using Newton's Method
	root := testPoly.NewtonRoot()
	fmt.Println(root)
}
