package main

import (
	"fmt"

	"gitlab.com/ollybritton/gomath"
)

func main() {
	// Get the factors of a number.
	factors := gomath.Factors(123)
	fmt.Println(factors)

	// Check if a number is prime.
	fmt.Println(gomath.IsPrime(10))   // false
	fmt.Println(gomath.IsPrime(2017)) // true

	// Get the prime factors of a number.
	primeFactors := gomath.PrimeFactors(1203)
	fmt.Println(primeFactors)

	// Find the greatest common divisor of two numbers.
	fmt.Println(gomath.GCD(11, 14))

	// Perform a Sieve of Eratosthenes to find primes within a range.
	fmt.Println(gomath.Sieve(1, 120))

	fmt.Println(gomath.Contains([]int{1, 2, 3, 4, 5, 6}, 10))

}
